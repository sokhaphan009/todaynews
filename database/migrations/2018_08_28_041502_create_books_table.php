<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isbn')->index()->nullable();
            $table->string('title')->index()->nullable();
            $table->string('slug')->default('');
            $table->text('description')->nullable();
            $table->date('date_of_publication')->nullable();
            $table->integer('category_id')->index()->nullable();
            $table->string('publisher')->nullable()->comment('the company that publish the book');
            $table->string('edition')->nullable()->comment('number of book publishing.');
            $table->integer('pages')->nullable()->comment('number of page.');
            $table->enum('level',['general','bachelor',12,11,10,9,8,7,6,5,4,3,2,1])->index()->nullable();
            $table->longText('content')->nullable();
            $table->text('image')->nullable();
            $table->text('pdf')->nullable();

            $table->enum('status', ['PUBLISHED', 'DRAFT'])->default('PUBLISHED');
            $table->date('date');
            $table->integer('view_count')->default(0)->nullable();
            $table->boolean('featured')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }
/*

$table->increments('id');
$table->integer('')->unsigned();
$table->text('');
$table->text('')->nullable();



*/


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
