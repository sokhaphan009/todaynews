<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test-img/{id}',function ($id){

    $pdf = new \App\Lib\Pdf( public_path("pdfs/{$id}.pdf"));
    foreach (range(1, $pdf->getNumberOfPages()) as $pageNumber) {

        $path = public_path("pdfs/img/{$id}");
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        $pdf->setPage($pageNumber)
            ->setOutputFormat('png')
            ->setCompressionQuality(100)
            ->saveImage(public_path("pdfs/img/{$id}/{$pageNumber}.png"));
    }
    return $id;
});

Route::get('single-book/{id}/{n}',function ($id,$n, Request $request){
    //dd($id,$n, $page);
    $page = $request->page;
    $error = 0;
    if($page >1) {
        $np = 5;
        $count_pages = ceil($n / $np);
        if($page>$count_pages){
            return '';
        }

        $from = ($page - 1) * $np + 1;
        $to = $from + $np - 1;
        $to = $to > $n ? $n : $to;

        $html = '';
        for ($k = $from; $k <= $to; $k++) {
            $html .= ' <div class="post">
                        <img src="' . asset("pdfs/img/{$id}/{$k}.png") . '">
                    </div>';
        }

        return $html;
    }

    return '';

});