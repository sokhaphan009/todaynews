<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('news.home');
});*/
Route::get('/','Client\NewsController@index');
Route::get('/category','Client\NewsController@list');
Route::get('/category-book','Client\NewsController@list_book');
Route::get('/single','Client\NewsController@single');
Route::get('/single-book','Client\NewsController@single_book');
Route::get('/course-outline','Client\NewsController@course_outline');
Route::get('/page','Client\NewsController@page');