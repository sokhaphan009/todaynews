<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes

    CRUD::resource('book', 'BookCrudController');
    CRUD::resource('article', 'ArticleCrudController');
    CRUD::resource('book-category', 'BookCategoryCrudController');
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('book-tag', 'BookTagCrudController');
    CRUD::resource('tag', 'TagCrudController');
    CRUD::resource('course-outlines', 'CourseOutlineCrudController');
    CRUD::resource('setting2', 'SettingCrudController');
    CRUD::resource('menu-item2', 'MenuItemCrudController');
    CRUD::resource('advertise', 'AdvertisingCrudController');

}); // this should be the absolute last line of this file


