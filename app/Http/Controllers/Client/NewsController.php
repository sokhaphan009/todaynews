<?php

namespace App\Http\Controllers\Client;

use App\Models\Article;
use App\Models\Book;
use App\Models\Category;
use App\Models\CourseOutline;
use App\Models\MenuItem;
use Backpack\PageManager\app\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function index()
    {
        return view('news.home');
    }

    public function list(Request $request)
    {
        $menu_id = $request->id;
        if ($menu_id > 0) {
            $m = MenuItem::find($menu_id);
            // dd($m);
            if ($m != null) {
                $cat_id = $m->category_id;
                if (is_array($cat_id)) {
                    $categories = Article::whereIn('category_id', $cat_id)->orderBy('id', 'desc')->paginate(18);
                    $categories->appends([
                        'id' => $menu_id
                    ]);
                    if (count($categories) > 0) {
                        return view('news.list', ['rows' => $categories, 'menu_id' => $menu_id]);
                    }

                }

            }

        }
        return redirect('/');
    }


    public function list_book(Request $request)
    {
        $menu_id = $request->id;
        if ($menu_id > 0) {
            $m = MenuItem::find($menu_id);
            // dd($m);
            if ($m != null) {
                $cat_id = $m->book_category_id;
                if (is_array($cat_id)) {
                    $categories = Book::whereIn('category_id', $cat_id)->orderBy('id', 'desc')->paginate(18);
                    $categories->appends([
                        'id' => $menu_id
                    ]);
                    if (count($categories) > 0) {
                        return view('news.list-book', ['rows' => $categories, 'menu_id' => $menu_id]);
                    }

                }

            }

        }
        return redirect('/');
    }



    public function single(Request $request)
    {
        $id = $request->id;
        if ($id > 0) {
            $m = Article::find($id);

            if ($m != null) {
                $m->view_count = $m->view_count >0?$m->view_count+1:1 ;
                $m->save();
                $categories = Article::where('category_id', $m->category_id)
                    ->where('id','<>',$id)
                    ->orderBy('id', 'desc')->limit(4)->get();

                return view('news.single', ['row' => $m,'categories' => $categories]);
            }

        }
        return redirect('/');
    }

    public function single_book(Request $request)
    {
        $id = $request->id;
        if ($id > 0) {
            $m = Book::find($id);

            if ($m != null) {
                $m->view_count = $m->view_count >0?$m->view_count+1:1 ;
                $m->save();
                $categories = Book::where('category_id', $m->category_id)
                    ->where('id','<>',$id)
                    ->orderBy('id', 'desc')->limit(4)->get();

                return view('news.single-book', ['row' => $m,'categories' => $categories]);
            }

        }
        return redirect('/');
    }


    public function course_outline(Request $request)
    {
        $id = $request->id;
        if ($id > 0) {
            $m = CourseOutline::find($id);

            if ($m != null) {
                $m->view_count = $m->view_count >0?$m->view_count+1:1 ;
                $m->save();
               /* $categories = Book::where('category_id', $m->category_id)
                    ->where('id','<>',$id)
                    ->orderBy('id', 'desc')->limit(4)->get();*/

                return view('news.course-outline', ['row' => $m]);//'categories' => $categories
            }

        }
        return redirect('/');
    }



    public function page(Request $request)
    {
        $id = $request->id;
        if ($id > 0) {
            $m = Page::find($id);

            if ($m != null) {
                /*$categories = Article::where('category_id', $m->category_id)
                    ->where('id','<>',$id)
                    ->orderBy('id', 'desc')->limit(4)->get();*/

                return view('news.page', ['row' => $m]);
            }

        }
        return redirect('/');
    }


}
