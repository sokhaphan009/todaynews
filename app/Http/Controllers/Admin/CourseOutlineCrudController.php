<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\TagRequest as StoreRequest;
use App\Http\Requests\TagRequest as UpdateRequest;

class CourseOutlineCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\\Models\\CourseOutline");
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/course-outlines');
        $this->crud->setEntityNameStrings('Course outlines', 'Course outlines');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
        */
        $this->crud->addColumn([
            'name' => 'image',
            'label' => 'Image',
            'type' => 'image2',
        ]);

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
                                'name' => 'name',
                                'label' => 'Name',
                            ]);

        $this->crud->addColumn([
                                'name' => 'description',
                                'label' => 'description',
                            ]);

        // ------ CRUD FIELDS
        $this->crud->addField([
                                'name' => 'name',
                                'label' => 'Name',
                                'wrapperAttributes' => [
                                    'class' => 'form-group col-md-4'
                                ],
                            ]);

        $this->crud->addField([
                                'name' => 'slug',
                                'label' => 'Slug (URL)',
                                'type' => 'text',
                                //'hint' => 'Will be automatically generated from your name, if left empty.',
                                'wrapperAttributes' => [
                                    'class' => 'form-group col-md-2'
                                ],
                                // 'disabled' => 'disabled'
                            ]);

        $this->crud->addField([
                                'name' => 'price',
                                'label' => 'Price',
                                'type' => 'number',
                                //'hint' => 'Will be automatically generated from your name, if left empty.',
                                'wrapperAttributes' => [
                                    'class' => 'form-group col-md-2'
                                ],
                                // 'disabled' => 'disabled'
                            ]);

        $this->crud->addField([
            'name' => 'event_date_range', // a unique name for this field
            'start_name' => 'start_date', // the db column that holds the start_date
            'end_name' => 'end_date', // the db column that holds the end_date
            'label' => 'Date Range',
            'type' => 'date_range',
            // OPTIONALS
            'start_default' => date('Y-m-d'), // default value for start_date
            'end_default' => date('Y-m-d'), // default value for end_date
            'date_range_options' => [ // options sent to daterangepicker.js
                'timePicker' => false,
                'locale' => ['format' => 'YYYY-MM-DD']
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
        ]);

        $this->crud->addField([    // WYSIWYG
            'name' => 'description',
            'label' => 'Description',
            'type' => 'ckeditor',
            'placeholder' => 'Your description text here',
        ]);

        $this->crud->addField([    // WYSIWYG
            'name' => 'course_outline_detail',
            'type' => 'view',
            'view' => 'partials/course_outline_detail'
        ]);


        $this->crud->addField([ // image
            'label' => 'Image',
            'name' => "image",
            'type' => 'image2',
            'default' => asset('No_Image_Available.jpg'),
            //'tab' => 'Image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 540 / 372, // ommit or set to 0 to allow any aspect ratio
            // 'prefix' => 'uploads/images/profile_pictures/' // in case you only store the filename in the database, this text will be prepended to the database value
        ]);
//1080 x 744
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
