<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AdvertisingRequest as StoreRequest;
use App\Http\Requests\AdvertisingRequest as UpdateRequest;

/**
 * Class AdvertisingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AdvertisingCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Advertising');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/advertise');
        $this->crud->setEntityNameStrings('advertising', 'advertisings');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        //$this->crud->setFromDb();
        // ------ CRUD FIELDS


        // ------ CRUD COLUMNS
       /* $this->crud->addColumn([
            'name' => 'image',
            'label' => 'Image',
            'type' => 'browse',
        ]);*/
        $this->crud->addColumn([
            'name' => 'title',
            'label' => 'Title',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'description',
            'label' => 'Description',
        ]);
        $this->crud->addColumn([
            'name' => 'link',
            'label' => 'Link',
        ]);
        $this->crud->addColumn([
            'name' => 'location',
            'label' => 'Location',
            'type' => 'enum'
        ]);
        $this->crud->addColumn([
            'name' => 'status',
            'label' => 'Status',
            'type' => 'enum'
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'title',
            'label' => 'Title',
            'type' => 'text',
            'placeholder' => 'Your title here',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ]);

        $this->crud->addField([
            'name' => 'link',
            'label' => 'Link (URL)',
            'type' => 'text',
            'placeholder' => 'please type url here',
            // 'disabled' => 'disabled'
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ]);

        $this->crud->addField([    // WYSIWYG
            'name' => 'description',
            'label' => 'Description',
            'type' => 'textarea',
            'placeholder' => 'Your description text here',
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'location',
            'label' => 'Location',
            'type' => 'enum',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ]);

        $this->crud->addField([
            'name' => 'status',
            'label' => 'Status',
            'type' => 'enum',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ]);

        $this->crud->addField([   // Browse multiple
            'name' => 'image',
            'label' => 'Image',
            'type' => 'browse_multiple',
            // 'multiple' => true, // enable/disable the multiple selection functionality
            // 'mime_types' => null, // visible mime prefixes; ex. ['image'] or ['application/pdf']
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
