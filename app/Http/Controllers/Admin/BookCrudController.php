<?php

namespace App\Http\Controllers\Admin;

use App\Models\BookCategory;
use App\Models\Category;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BookRequest as StoreRequest;
use App\Http\Requests\BookRequest as UpdateRequest;

class BookCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\\Models\\Book");
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin') . '/book');
        $this->crud->setEntityNameStrings('book', 'books');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
        */
        /*$this->crud->addFilter([ // select2 filter
            'name' => 'category_id',
            'type' => 'select2',
            'label'=> 'Category'
        ], function() {
            return Category::all()->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'category_id', $value);
        });*/
        
        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'image',
            'label' => 'Image',
            'type' => 'image2',
        ]);
        $this->crud->addColumn([
            'name' => 'date',
            'label' => 'Date',
            'type' => 'date',
        ]);
        $this->crud->addColumn([
            'name' => 'status',
            'label' => 'Status',
        ]);

        $this->crud->addColumn([
            'name' => 'title',
            'label' => 'Title',
        ]);
        $this->crud->addColumn([
            'name' => 'view_count',
            'label' => 'view count',
           // 'type' => 'check',
        ]);
        $this->crud->addColumn([
            'name' => 'featured',
            'label' => 'Featured',
            'type' => 'check',
        ]);

        $this->crud->addColumn([
            'label' => 'Category',
            'type' => 'select',
            'name' => 'category_id',
            'entity' => 'category',
            'attribute' => 'name',
            'model' => "App\\Models\\BookCategory",
        ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
            'name' => 'title',
            'label' => 'Title',
            'type' => 'text',
            'placeholder' => 'Your title here',
        ]);
        $this->crud->addField([    // WYSIWYG
            'name' => 'description',
            'label' => 'Description',
            //'type' => 'ckeditor',
            'placeholder' => 'Your description text here',
        ]);
        $this->crud->addField([
            'name' => 'slug',
            'label' => 'Slug (URL)',
            'type' => 'text',
            'hint' => 'Will be automatically generated from your title, if left empty.',
            // 'disabled' => 'disabled'
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'date',
            'label' => 'Date',
            'type' => 'date_picker',
            'value' => date('Y-m-d'),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ], 'create');
        $this->crud->addField([    // TEXT
            'name' => 'date',
            'label' => 'Date',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ], 'update');


        $this->crud->addField([    // WYSIWYG
            'name' => 'content',
            'label' => 'Content',
            'type' => 'ckeditor',
            'placeholder' => 'Your textarea text here',
        ]);

        $this->crud->addField([ // image
            'label' => 'Image',
            'name' => "image",
            'type' => 'image2',
            'default' => asset('No_Image_Available.jpg'),
            //'tab' => 'Image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 540 / 372, // ommit or set to 0 to allow any aspect ratio
            // 'prefix' => 'uploads/images/profile_pictures/' // in case you only store the filename in the database, this text will be prepended to the database value
        ]);

        $this->crud->addField([    // SELECT
            'label' => 'Category',
            'type' => 'select2_article_category',
            'name' => 'category_id',
            'default' => session('sess_category_id'),
            'entity' => 'category',
            'attribute' => 'name',
            'model' => "App\\Models\\BookCategory",
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
        ]);
        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            'label' => 'Tags',
            'type' => 'select2_multiple',
            'name' => 'tags', // the method that defines the relationship in your Model
            'entity' => 'tags', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\\Models\\BookTag", // foreign key model
            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
        ]);
        $this->crud->addField([    // ENUM
            'name' => 'status',
            'label' => 'Status',
            'type' => 'enum',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
        ]);

        $this->crud->addField([    // ENUM
            'name' => 'level',
            'label' => 'Level',
            'type' => 'enum',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
        ]);


        $this->crud->addField([    // CHECKBOX
            'name' => 'featured',
            'label' => 'Featured item',
            'type' => 'checkbox',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
        ]);
        $this->crud->orderBy('id', 'desc');
        $this->crud->addButtonFromModelFunction('line', 'addButtonCustom', 'addButtonCustom', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model

        //$this->crud->enableAjaxTable();
    }

    public function store(StoreRequest $request)
    {
        $cat_id = $request->category_id;
        session([
            'sess_category_id'=>$cat_id
        ]);
        if($cat_id >0){
            $m = BookCategory::find($cat_id);
            if($m != null){
                $used_order = $m->used_order == null ? 1: $m->used_order+1;
                $m->used_order = $used_order;
                $m->save();
            }
        }
        return parent::storeCrud();

    }

    public function update(UpdateRequest $request)
    {
        $cat_id = $request->category_id;

        session([
            'sess_category_id'=>$cat_id
        ]);

        if($cat_id >0){
            $m = BookCategory::find($cat_id);
            if($m != null){
                $used_order = $m->used_order == null ? 1: $m->used_order+1;
                $m->used_order = $used_order;
                $m->save();
            }
        }
        return parent::updateCrud();
    }
}
