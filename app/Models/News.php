<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'articles';

    public static function getDynamicSlide(){//view_count
        $dyn_slide = Article::orderBy('view_count','desc')->limit(9)->get();
        return $dyn_slide->chunk(5);
    }
    public static function getStaticSlide(){//view_count
        $staic_slide = Article::orderBy('id','desc')->limit(3)->get();
        return $staic_slide->chunk(2);
    }

    public static function getHotNews(){//view_count
        $hot_news = Article::where('featured','1')->orderBy('id','desc')->limit(4)->get();
        return $hot_news;
    }
    public static function getHomeLyfeStyle1(){//view_count
        $m = getSetting();
        $n = getSettingKey('home-style-1', $m );
        $arr = [];
        if($n != null){
            $arr = json_decode($n);
        }

        $hot_news = Article::whereIn('category_id',$arr)->orderBy('id','desc')->limit(7)->get();
        return $hot_news->chunk(6);
    }
    public static function getHomeLyfeStyle2(){//view_count
        $m = getSetting();
        $n = getSettingKey('home-style-2', $m );
        $arr = [];
        if($n != null){
            $arr = json_decode($n);
        }

        $hot_news = Article::whereIn('category_id',$arr)->orderBy('id','desc')->limit(7)->get();
        return $hot_news->chunk(4);
    }
    public static function getHomeLyfeStyle3(){//view_count
        $m = getSetting();
        $n = getSettingKey('home-style-3', $m );
        $arr = [];
        if($n != null){
            $arr = json_decode($n);
        }

        $hot_news = Article::whereIn('category_id',$arr)->orderBy('id','desc')->limit(5)->get();
        return $hot_news->chunk(3);
    }
    public static function getHomeLyfeStyle4(){//view_count
        $m = getSetting();
        $n = getSettingKey('home-style-4', $m );
        $arr = [];
        if($n != null){
            $arr = json_decode($n);
        }

        $hot_news = Article::whereIn('category_id',$arr)->orderBy('id','desc')->limit(5)->get();
        return $hot_news->chunk(3);
    }

    public static function getAdvertise1(){
        $adv = Advertising::where('location','location1')->orderBy('id','desc')->limit(4)->get();
         return $adv;
    }
    public static function getAdvertise2(){
        $adv = Advertising::where('location','location2')->orderBy('id','desc')->get();
        return $adv;
    }
    public static function getAdvertise3(){
        $adv = Advertising::where('location','location3')->orderBy('id','desc')->first();
        return $adv;
    }
    public static function getCategoryTitle($t){//view_count
        $m = getSetting();
        $n = getSettingKey('home-style-'.$t, $m );
        $arr = [];
        if($n != null){
            $arr = json_decode($n);
        }
        if(count($arr)>0){
            $c = Category::select('name')->whereIn('id',$arr)->get();
            if(count($c)>0){
                $title = [];
                foreach ($c as $r){
                    $title[] = $r->name;
                }
                if(count($title)>0){
                    return implode(' / ',$title);
                }
            }
        }
        return '';
    }

}
