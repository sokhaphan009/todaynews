<?php

namespace App\Lib\Exceptions;

use Exception;

class PageDoesNotExist extends Exception
{
}
