<?php

namespace App\Lib\Exceptions;

use Exception;

class PdfDoesNotExist extends Exception
{
}
