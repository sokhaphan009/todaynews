<?php
function _getFileName($file_name)
{
    $newfile = basename($file_name);
    if (strpos($newfile,'\\') !== false)
    {
        $tmp = preg_split("[\\\]",$newfile);
        $newfile = $tmp[count($tmp) - 1];
        return($newfile);
    }
    else
    {
        return($file_name);
    }
}
/*function get_basename($filename)
{
    return preg_replace('/^.+[\\\\\\/]/', '', $filename);
}*/
function _getImage($url_img,$template="small"){
    $file_name =basename($url_img);

    return url("images/{$template}/{$file_name}");
}
function  _getCategoryName($id){
    $cat_name = \App\Models\Category::select('name')->where('id',$id)->first();
    if($cat_name != "" || $cat_name != null){
        return $cat_name;
    }
}

function getSetting($key = null)
{
    if ($key == null) {
        $m = \App\Models\Setting::select('key', 'value')->get();
    } else {
        $m = \App\Models\Setting::where('key', $key)->select('key', 'value')->first();
    }

    return $m;
}

function getSettingKey($key, $m = null)
{
    if ($m != null) {
        if (isset($m->key)) {
            return $m->value;
        } else {
            foreach ($m as $row) {
                if ($row->key == $key) {
                    return $row->value;
                }
            }
        }
    }

    return null;
}


function getDescriptionStudy($description,$title = '',$start_date = '',$price = 'Free'){
    $s = str_replace('{{title}}',$title,$description);
    $s = str_replace('{{start_date}}',$start_date,$s);
    $s = str_replace('{{price}}',$price,$s);

    return $s;
}