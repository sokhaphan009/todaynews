<span>
  @if( !empty($entry->{$column['name']}) )
      <?php
      $url = _getImage($entry->{$column['name']});
      $url_original = _getImage($entry->{$column['name']},'original');
      ?>
    <a
      href="{{ $url_original }}"
      target="_blank"
    >
      <img
        src="{{ $url }}"
        style="
          max-height: {{ isset($column['height']) ? $column['height'] : "25px" }};
          width: {{ isset($column['width']) ? $column['width'] : "auto" }};
          border-radius: 3px;"
      />
    </a>
  @else
    -
  @endif
</span>
