<?php
$days = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
];

$study_date = isset($crud->entry->study_date)?($crud->entry->study_date):[];
//dd($study_date);
?>

<table>
    @foreach($days as $day)
    <tr>
        <td><label for="checkbox_{{$day}}"><input
                    @if(isset($study_date[$day]['checkbox']))    checked="checked"  @endif
                        id="checkbox_{{$day}}" type="checkbox" name="study_date[{{$day}}][checkbox]" value="1">
                {{$day}} &nbsp;&nbsp;&nbsp;</label></td>
        <td><input @if(isset($study_date[$day]['from']))    value="{{$study_date[$day]['from']}}"  @endif class="form-control" type="time" name="study_date[{{$day}}][from]" placeholder="From Time"></td>
        <td><input @if(isset($study_date[$day]['to']))    value="{{$study_date[$day]['to']}}"  @endif class="form-control" type="time" name="study_date[{{$day}}][to]" placeholder="To Time"></td>
    </tr>
    @endforeach
</table>
