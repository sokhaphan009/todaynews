@extends('news.main')

@section('main')

    <div class="main-content">
        @yield('content')

    </div>
    <!-- /Main Content -->

    <!-- Left Sidebar -->
    <div class="column-one-third">
        {{--
                        <div class="sidebar">
                            <h5 class="line"><span>Stay Connected.</span></h5>
                            <ul class="social">
                                <li>
                                    <a href="#" class="facebook"><i class="icon-facebook"></i></a>
                                    <span>6,800 <br/> <i>fans</i></span>
                                </li>
                                <li>
                                    <a href="#" class="twitter"><i class="icon-twitter"></i></a>
                                    <span>12,475 <br/> <i>followers</i></span>
                                </li>
                                <li>
                                    <a href="#" class="rss"><i class="icon-rss"></i></a>
                                    <span><i>Subscribe via rss</i></span>
                                </li>
                            </ul>
                        </div>
--}}

        <div class="sidebar">
            <h5 class="line"><span>MekongMall</span></h5>
            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fmekongmall%2Fvideos%2F2106005486313973%2F&show_text=1&width=560" width="100%"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>
            {{--<iframe src="http://player.vimeo.com/video/65110834?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="300px" height="170px" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
            --}}
        </div>


        @php
            $advs = \App\Models\News::getAdvertise1();
            $advs2 = \App\Models\News::getAdvertise2();
        @endphp
        @if(count($advs2)>0)
            <div class="sidebar">
                @foreach($advs2 as $adv)
                    @php
                        $img = $adv->image;
                        $image = isset($img[0])?asset($img[0]):"{$base_news_url}/img/tf_300x250_v1.gif";
                    @endphp
                    <h5 class="line"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{--{{$adv->title}}--}}</span></h5>
                    <a href="{{$adv->link}}" target="_blank"><img src="{{$image}}" width="100%" alt="{{$adv->description}}" /></a>
                @endforeach
            </div>
        @endif
        @if(count($advs)>0)
            <div class="sidebar">
                <h5 class="line"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{--Ads Spot.--}}</span></h5>
                <ul class="ads125">
                    @foreach($advs as $adv)
                        @php
                            $img = $adv->image;
                            $image = isset($img[0])?asset($img[0]):"{$base_news_url}/img/tf_300x250_v1.gif";


                        @endphp
                        <li><a href="{{$adv->link}}" target="_blank"><img src="{{$image}}" alt="{{$adv->description}}" /></a></li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{--
                                <div class="sidebar">
                                    <div id="tabs">
                                        <ul>
                                            <li><a href="#tabs1">Recent.</a></li>
                                            <li><a href="#tabs2">Popular.</a></li>
                                            <li><a href="#tabs3">Comments.</a></li>
                                        </ul>
                                        <div id="tabs1">
                                            <ul>
                                                <li>
                                                    <a href="#" class="title">Blandit Rutrum, Erat et Sagittis Adipcising Elit.</a>
                                                    <span class="meta">26 May, 2013.   \\   <a href="#">World News.</a>   \\   <a href="#">No Coments.</a></span>
                                                    <span class="rating"><span style="width:70%;"></span></span>
                                                </li>
                                                <li>
                                                    <a href="#" class="title">Blandit Rutrum, Erat et Sagittis Adipcising Elit.</a>
                                                    <span class="meta">26 May, 2013.   \\   <a href="#">World News.</a>   \\   <a href="#">No Coments.</a></span>
                                                    <span class="rating"><span style="width:70%;"></span></span>
                                                </li>
                                                <li>
                                                    <a href="#" class="title">Blandit Rutrum, Erat et Sagittis Adipcising Elit.</a>
                                                    <span class="meta">26 May, 2013.   \\   <a href="#">World News.</a>   \\   <a href="#">No Coments.</a></span>
                                                    <span class="rating"><span style="width:70%;"></span></span>
                                                </li>
                                                <li>
                                                    <a href="#" class="title">Blandit Rutrum, Erat et Sagittis Adipcising Elit.</a>
                                                    <span class="meta">26 May, 2013.   \\   <a href="#">World News.</a>   \\   <a href="#">No Coments.</a></span>
                                                    <span class="rating"><span style="width:70%;"></span></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="tabs2">
                                            <ul>
                                                <li>
                                                    <a href="#" class="title">Mauris eleifend est et turpis. Duis id erat.</a>
                                                    <span class="meta">27 May, 2013.   \\   <a href="#">World News.</a>   \\   <a href="#">No Coments.</a></span>
                                                    <span class="rating"><span style="width:70%;"></span></span>
                                                </li>
                                                <li>
                                                    <a href="#" class="title">Mauris eleifend est et turpis. Duis id erat.</a>
                                                    <span class="meta">27 May, 2013.   \\   <a href="#">World News.</a>   \\   <a href="#">No Coments.</a></span>
                                                    <span class="rating"><span style="width:70%;"></span></span>
                                                </li>
                                                <li>
                                                    <a href="#" class="title">Mauris eleifend est et turpis. Duis id erat.</a>
                                                    <span class="meta">27 May, 2013.   \\   <a href="#">World News.</a>   \\   <a href="#">No Coments.</a></span>
                                                    <span class="rating"><span style="width:70%;"></span></span>
                                                </li>
                                                <li>
                                                    <a href="#" class="title">Mauris eleifend est et turpis. Duis id erat.</a>
                                                    <span class="meta">27 May, 2013.   \\   <a href="#">World News.</a>   \\   <a href="#">No Coments.</a></span>
                                                    <span class="rating"><span style="width:70%;"></span></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="tabs3">
                                            <ul>
                                                <li>
                                                    <a href="#" class="title"><strong>Someone:</strong> eleifend est et turpis. Duis id erat.Mauris eleifend est et turpis. Duis id erat.</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="title"><strong>Someone:</strong> eleifend est et turpis. Duis id erat.Mauris eleifend est et turpis. Duis id erat.</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="title"><strong>Someone:</strong> eleifend est et turpis. Duis id erat.Mauris eleifend est et turpis. Duis id erat.</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="title"><strong>Someone:</strong> eleifend est et turpis. Duis id erat.Mauris eleifend est et turpis. Duis id erat.</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="title"><strong>Someone:</strong> eleifend est et turpis. Duis id erat.Mauris eleifend est et turpis. Duis id erat.</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                --}}
        {{--
                                <div class="sidebar">
                                    <h5 class="line"><span>Accordion.</span></h5>
                                    <div id="accordion">

                                        <h3>Poserue Clubre.</h3>
                                        <div>
                                            <p>Vestibulum tempor feugiat est in posuere. Sed auctor libero augue, a faucibus turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices. posuere.</p>
                                        </div>

                                        <h3>Lubelia Mest.</h3>
                                        <div>
                                            <p>Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.</p>
                                        </div>

                                        <h3>Tincidunt Massa.</h3>
                                        <div>
                                            <p>Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac liberoac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</p>
                                        </div>

                                        <h3>Quisque lobortis.</h3>
                                        <div>
                                            <p>Cras dictum. Pellentesque habitant morbi tristique senectus et netuset malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia mauris vel est.</p>
                                        </div>

                                    </div>
                                </div>

                                --}}

        <div class="sidebar">
            <h5 class="line"><span>Facebook.</span></h5>

            {{--<iframe src="https://www.facebook.com/Cxpress-292901028118523/?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=298&amp;height=258&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;border_color=%23BCBCBC&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:298px; height:258px;" allowTransparency="true"></iframe>
        --}}
            <div class="fb-page" data-href="https://www.facebook.com/Cxpress-292901028118523/" data-width="298" data-height="258" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Cxpress-292901028118523/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Cxpress-292901028118523/">Cxpress</a></blockquote></div><script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=2030832340275457&autoLogAppEvents=1';
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>

        </div>
    </div>
    <!-- /Left Sidebar -->

@endsection