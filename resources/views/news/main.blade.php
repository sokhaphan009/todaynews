<!DOCTYPE html>
<?php
$detect = new \App\Helpers\Mobile_Detect();
?>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="We provide all products such as Online Fashion Shop​, WOMEN COLLECTIONS Shop​, Online Blouses &amp; Shirts, Online Dresses Shop​, Online Jeans Shop​, Online MEN COLLECTIONS, Online Electronics, Online Furniture, Online Bathroom, Online Living room, Online Bedroom, Online Home &amp; Lights"/>

    <meta name="author" content="Son Sothea">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    @section('fb_meta')
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Online shopping mall in Cambodia, Fashion, Electronics, Furniture, Cloths" />
    <meta property="og:description" content="We provide all products such as Online Fashion Shop​, WOMEN COLLECTIONS Shop​, Online Blouses &amp; Shirts, Online Dresses Shop​, Online Jeans Shop​, Online MEN COLLECTIONS, Online Electronics, Online Furniture, Online Bathroom, Online Living room, Online Bedroom, Online Home &amp; Lights" />
    <meta property="og:url" content="https://mekongmalls.com/" />
    <meta property="og:site_name" content="Online Shopping Mall Cambodia" />
    <meta property="fb:app_id" content="389475391479285" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="We provide all products such as Online Fashion Shop​, WOMEN COLLECTIONS Shop​, Online Blouses &amp; Shirts, Online Dresses Shop​, Online Jeans Shop​, Online MEN COLLECTIONS, Online Electronics, Online Furniture, Online Bathroom, Online Living room, Online Bedroom, Online Home &amp; Lights" />
    <meta name="twitter:title" content="Online shopping mall in Cambodia, Fashion, Electronics, Furniture, Cloths" />

    @show
    <title>@yield('title')</title>

    <link rel="shortcut icon" href="{{$base_news_url}}/img/sms-4.ico" />

    <!-- STYLES -->
    <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/superfish.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/fontello/fontello.css" />
    <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/flexslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/ui.css" />
    <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/base.css" />
    <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/style.css" />
    <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/960.css?id=10" />
    <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/devices/1000.css" media="only screen and (min-width: 768px) and (max-width: 1000px)" />
    <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/devices/767.css" media="only screen and (min-width: 480px) and (max-width: 767px)" />
    <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/devices/479.css" media="only screen and (min-width: 200px) and (max-width: 479px)" />
    <link href='http://fonts.googleapis.com/css?family=Merriweather+Sans:400,300,700,800' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]> <script type="text/javascript" src="{{$base_news_url}}/js/customM.js"></script> <![endif]-->
    @yield('css')

    <link href="https://fonts.googleapis.com/css?family=Hanuman&amp;subset=khmer" rel="stylesheet">

    <style>
        body{font-family: 'Hanuman', serif;}
    </style>

    @if ( $detect->isMobile() )
        <link rel="stylesheet" type="text/css" href="{{$base_news_url}}/css/style_menu2.css?id=1" />
        <style>
            h5.line>span{
                border-bottom:0px !important;

            }
            .flexslider{
                margin-top: 20px !important;
            }
            .main-slider{
                width: 100% !important;
            }

            .relatednews ul li img,.relatednews ul li{
                max-width: 100% !important;
                width: 100% !important;
            }

            ul.ads125 li a img{
                width: 100% !important;
                height: auto !important;
            }
        </style>
    @endif

</head>

<body>
@section('fb')
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=2030832340275457&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@show

@if ( $detect->isMobile() )
    <div class="mainWrap" style="margin-top: 0 !important;">
{{--
        <h1 class="title">Responsive Multi-level Flat Menu</h1>
        <p class="back">Resize the browser window so you can see the different menu layouts.</p>
        <p class="back"><a href="http://www.flashuser.net/responsive-flat-menu">Go back to article</a></p>--}}
        <a id="touch-menu" class="mobile-menu" href="#"><i class="icon-reorder"></i>Menu</a>

{{--
        <nav>
            <ul class="menu">
                <li><a href="#"><i class="icon-home"></i>HOME</a>
                    <ul class="sub-menu">
                        <li><a href="#">Sub-Menu 1</a></li>
                        <li><a href="#">Sub-Menu 2</a></li>
                        <li><a href="#">Sub-Menu 3</a></li>
                    </ul>
                </li>
                <li><a  href="#"><i class="icon-user"></i>ABOUT</a></li>
                <li><a  href="#"><i class="icon-camera"></i>PORTFOLIO</a>
                    <ul class="sub-menu">
                        <li><a href="#">Sub-Menu 1</a></li>
                        <li><a href="#">Level 3 Menu</a>
                            <ul>
                                <li><a href="#">Sub-Menu 4</a></li>
                                <li><a href="#">Sub-Menu 5</a></li>
                                <li><a href="#">Sub-Menu 6</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a  href="#"><i class="icon-bullhorn"></i>BLOG</a></li>
                <li><a  href="#"><i class="icon-envelope-alt"></i>CONTACT</a></li>
            </ul>
        </nav>--}}



        <!-- Nav -->
        <nav>
            <ul class="menu">
                <?php
                $_menu_id = isset($menu_id)?$menu_id:0;
                ?>
                @foreach(\App\Models\MenuItem::whereNull('parent_id')->orderBy('lft','ASC')->get() as $menue)
                    @php

                        $m_url = '';
                        if($menue->type == 'page_link'){
                            $m_url = url("page?id={$menue->page_id}");
                        }elseif ($menue->type == 'internal_link'){
                            $m_url = url($menue->link);
                        }elseif ($menue->type == 'external_link'){
                            $m_url = $menue->link;
                        }elseif ($menue->type == 'category_link'){
                            $m_url = url("category?id={$menue->id}");
                        }elseif ($menue->type == 'book_category_link'){
                            $m_url = url("category-book?id={$menue->id}");
                        }
                    @endphp
                    <li class="{{ ($_menu_id == $menue->id?'current':'') }}"><a href="{{$m_url}}">{{$menue->name}}</a>
                        @php
                            $sub_m = \App\Models\MenuItem::where('parent_id',$menue->id)->orderBy('lft','ASC')->get();
                        @endphp
                        @if(count($sub_m)>0)
                            @foreach($sub_menu as $sub)
                                <ul class="sub-menu">
                                    @foreach($sub_m as $rs_m)
                                        @php
                                            $m_url = '';
                                             if($rs_m->type == 'page_link'){
                                                $m_url = url("page?id={$rs_m->page_id}");
                                            }elseif ($rs_m->type == 'internal_link'){
                                                $m_url = url($rs_m->link);
                                            }elseif ($rs_m->type == 'external_link'){
                                                $m_url = $rs_m->link;
                                            }elseif ($rs_m->type == 'category_link'){
                                                $m_url = url("category?id={$rs_m->id}");
                                            }elseif ($rs_m->type == 'book_category_link'){
                                                $m_url = url("category-book?id={$rs_m->id}");
                                            }
                                        @endphp
                                        <li><a href="{{$m_url}}">{{$rs_m->name}}</a></li>
                                    @endforeach
                                </ul>
                            @endforeach
                        @endif
                        {{--<li>
                            <a href="#">Pages.</a>
                            <ul>
                                <li><i class="icon-right-open"></i><a href="leftsidebar.html">Left Sidebar.</a></li>
                                <li><i class="icon-right-open"></i><a href="reviews.html">Reviews.</a></li>
                                <li><i class="icon-right-open"></i><a href="single.html">Single News.</a></li>
                                <li><i class="icon-right-open"></i><a href="features.html">Features.</a></li>
                                <li><i class="icon-right-open"></i><a href="contact.html">Contact.</a></li>
                            </ul>
                        </li>--}}
                        @endforeach
                    </li>
            </ul>

        </nav>
        <!-- /Nav -->
    </div>
@endif
<!-- Body Wrapper -->
<div class="body-wrapper">
    <div class="controller">
        <div class="controller2">
            @if ( !$detect->isMobile() )

            <!-- Header -->
            <header id="header">
                <div class="container">
                    <div class="column">

                        <div class="logo">
                            <a href="{{url('/')}}"><img src="news/img/logo.jpg" alt="LOGO" height="65" /></a>
                        </div>
                        @php
                            $adv3 = \App\Models\News::getAdvertise3();
                        @endphp
                        @if($adv3 != null)
                        <div class="advertise3" style="float: right; margin-top:10px;">

                                @php
                                    $img = $adv3->image;
                                    $image = isset($img[0])?asset($img[0]):"{$base_news_url}/img/tf_300x250_v1.gif";


                                @endphp
                                <li><a href="{{$adv3->link}}"><img src="{{$image}}" alt="{{$adv3->description}}" /></a></li>
                        </div>
                        @endif
                        {{--<div class="search">

                            <form action="#" method="post">
                                <input type="text" value="Search." onblur="if(this.value=='') this.value='Search.';" onfocus="if(this.value=='Search.') this.value='';" class="ft"/>
                                <input type="submit" value="" class="fs">
                            </form>
                        </div>
--}}
                        <!-- Nav -->
                        <nav id="nav">
                            <ul class="sf-menu">
                                <?php
                                $_menu_id = isset($menu_id)?$menu_id:0;
                                ?>
                                @foreach(\App\Models\MenuItem::whereNull('parent_id')->orderBy('lft','ASC')->get() as $menue)
                                    @php

                                        $m_url = '';
                                        if($menue->type == 'page_link'){
                                            $m_url = url("page?id={$menue->page_id}");
                                        }elseif ($menue->type == 'internal_link'){
                                            $m_url = url($menue->link);
                                        }elseif ($menue->type == 'external_link'){
                                            $m_url = $menue->link;
                                        }elseif ($menue->type == 'category_link'){
                                            $m_url = url("category?id={$menue->id}");
                                        }elseif ($menue->type == 'book_category_link'){
                                            $m_url = url("category-book?id={$menue->id}");
                                        }
                                    @endphp
                                    <li class="{{ ($_menu_id == $menue->id?'current':'') }}"><a href="{{$m_url}}">{{$menue->name}}</a>
                                    @php
                                        $sub_m = \App\Models\MenuItem::where('parent_id',$menue->id)->orderBy('lft','ASC')->get();
                                    @endphp
                                        @if(count($sub_m)>0)
                                            @foreach($sub_menu as $sub)
                                                <ul>
                                                    @foreach($sub_m as $rs_m)
                                                        @php
                                                            $m_url = '';
                                                             if($rs_m->type == 'page_link'){
                                                                $m_url = url("page?id={$rs_m->page_id}");
                                                            }elseif ($rs_m->type == 'internal_link'){
                                                                $m_url = url($rs_m->link);
                                                            }elseif ($rs_m->type == 'external_link'){
                                                                $m_url = $rs_m->link;
                                                            }elseif ($rs_m->type == 'category_link'){
                                                                $m_url = url("category?id={$rs_m->id}");
                                                            }elseif ($rs_m->type == 'book_category_link'){
                                                                $m_url = url("category-book?id={$rs_m->id}");
                                                            }
                                                        @endphp
                                                        <li><a href="{{$m_url}}">{{$rs_m->name}}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endforeach
                                        @endif
                                {{--<li>
                                    <a href="#">Pages.</a>
                                    <ul>
                                        <li><i class="icon-right-open"></i><a href="leftsidebar.html">Left Sidebar.</a></li>
                                        <li><i class="icon-right-open"></i><a href="reviews.html">Reviews.</a></li>
                                        <li><i class="icon-right-open"></i><a href="single.html">Single News.</a></li>
                                        <li><i class="icon-right-open"></i><a href="features.html">Features.</a></li>
                                        <li><i class="icon-right-open"></i><a href="contact.html">Contact.</a></li>
                                    </ul>
                                </li>--}}
                                @endforeach
                                </li>
                            </ul>

                        </nav>
                        <!-- /Nav -->
                    </div>
                </div>
            </header>
            <!-- /Header -->
            @endif
            <!-- Slider -->
            @yield('slider')
            <!-- / Slider -->

            <!-- Content -->
            <section id="content">
                <div class="container">
                    <!-- Main Content -->
                    @yield('main')


                </div>
            </section>
            <!-- / Content -->

            <!-- Footer -->
            <footer id="footer">
                <div class="container">
                    @yield('footer')
                    <div class="column-one-fourth">
                        <h5 class="line"><span>អំពីយើង</span></h5>
                        <p>
                            ក្លាយជារចនាសម្ព័ន្ធពាណិជ្ជកម្មតាមបច្ចេកវិទ្យាទំនើបដែលអាចអោយអតិថិជនរបស់យើងជួបប្រជុំគ្នាធ្វើអាជីវកម្មបានផលចំនេញច្រើននៅលើប្រព័ន្ធក្រុមហ៊ុនមេគង្គម៉ល (Mekong Mall) ហើយឆ្ពោះទៅជាក្រុមហ៊ុនធំមួយនៅអាស៊ីក្នុងរយះពេល១០ឆ្នាំខាងមុខ។
                        </p>
                    </div>
                    <div class="column-one-fourth">
                        <h5 class="line"><span>Tweets.</span></h5>
                        <div id="tweets"></div>
                    </div>
                    <div class="column-one-fourth">
                        <h5 class="line"><span>Navigation.</span></h5>
                        <ul class="footnav">
                            <li><a href="#"><i class="icon-right-open"></i> World.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Business.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Politics.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Sports.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Health.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Sciences.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Spotlight.</a></li>
                        </ul>
                    </div>
                    <div class="column-one-fourth">
                        <h5 class="line"><span>Flickr Stream.</span></h5>
                       
                        <ul class="footnav">
                            <li><a href="#"><i class="icon-right-open"></i> World.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Business.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Politics.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Sports.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Health.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Sciences.</a></li>
                            <li><a href="#"><i class="icon-right-open"></i> Spotlight.</a></li>
                        </ul>
                        
                    </div>
                   
                    <p class="copyright">Copyright 2018. All Rights Reserved</p>
                </div>
            </footer>
            <!-- / Footer -->

        </div>
    </div>
</div>
<!-- / Body Wrapper -->


<!-- SCRIPTS -->
<script type="text/javascript" src="{{$base_news_url}}/js/jquery.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/easing.min.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/1.8.2.min.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/ui.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/carouFredSel.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/superfish.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/customM.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/flexslider-min.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/tweetable.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/timeago.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/jflickrfeed.min.js"></script>
<script type="text/javascript" src="{{$base_news_url}}/js/mobilemenu.js"></script>

<!--[if lt IE 9]> <script type="text/javascript" src="{{$base_news_url}}/js/html5.js"></script> <![endif]-->
<script type="text/javascript" src="{{$base_news_url}}/js/mypassion.js?v=189797"></script>
@yield('js')

<script>

    $(window).bind('load', function() {
        $('img').each(function() {
            if((typeof this.naturalWidth != "undefined" &&
                this.naturalWidth == 0 )
                || this.readyState == 'uninitialized' ) {
                $(this).remove();
            }
        });
    });
</script>
@if ( $detect->isMobile() )
    <script>
        $(document).ready(function(){
            var touch 	= $('#touch-menu');
            var menu 	= $('.menu');

            $(touch).on('click', function(e) {
                e.preventDefault();
                menu.slideToggle();
            });

            $(window).resize(function(){
                var w = $(window).width();
                if(w > 767 && menu.is(':hidden')) {
                    menu.removeAttr('style');
                }
            });

        });
    </script>
@endif
</body>
</html>
