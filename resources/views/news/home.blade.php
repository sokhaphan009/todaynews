@extends('news.layout')

@section('title','Home')
@section('slider')
    <?php
    $detect = new \App\Helpers\Mobile_Detect();

    ?>
    <section id="slider">
        <div class="container">
            <div class="main-slider">
               {{-- <div class="badg">
                    <p><a href="#">ពត៌មានពេញនិយម</a></p>
                </div>--}}
                <div class="flexslider">
                    <ul class="slides">
                        @php
                            $dyn_slide = \App\Models\News::getDynamicSlide();
                            $dyn_slide2 = \App\Models\News::getStaticSlide();
                            $hot_news = \App\Models\News::getHotNews();
                            $get_home1 = \App\Models\News::getHomeLyfeStyle1();
                            $get_home2 = \App\Models\News::getHomeLyfeStyle2();
                            $get_home3 = \App\Models\News::getHomeLyfeStyle3();
                            $get_home4 = \App\Models\News::getHomeLyfeStyle4();
                        @endphp
                        @if(isset($dyn_slide[0]))
                            @if(count($dyn_slide[0])>0)
                                @foreach($dyn_slide[0] as $row)
                                    <li>
                                        <img src="{{_getImage($row->image,"large")}}" @if ( !$detect->isMobile() ) height="371px" @else width="100%" @endif alt="MyPassion" />
                                        <p class="flex-caption"><a href="{{url("single?id={$row->id}")}}">{{$row->title}}</a> {{$row->description}} </p>
                                    </li>
                                @endforeach
                            @endif
                        @endif
                    </ul>
                </div>
            </div>

            <div class="slider2">
                @if(isset($dyn_slide2[1]))
                    @if(count($dyn_slide2[1])>0)
                        @foreach($dyn_slide2[1] as $row)
                            {{--<div class="badg">
                                --}}{{--<p><a href="{{url("single?id={$row->id}")}}">Latest.</a></p>--}}{{--
                            </div>--}}
                            <a href="{{url("single?id={$row->id}")}}"><img src="{{_getImage($row->image,"large")}}"  @if ( !$detect->isMobile() ) width="380px" height="217px" @endif alt="{{$row->description}}" /></a>
                            <p class="caption"><a href="{{url("single?id={$row->id}")}}">{{$row->title}}</a> {{$row->description}} </p>

                        @endforeach
                    @endif
                @endif

            </div>
            @if(isset($dyn_slide2[0]))
                @if(count($dyn_slide2[0])>0)
                    @foreach($dyn_slide2[0] as $row)
                        <div class="slider3">
                            <a href="{{url("single?id={$row->id}")}}"><img src="{{_getImage($row->image,"small")}}" width="180px" height="135px" alt="{{$row->description}}" /></a>
                            <p class="caption"><a href="{{url("single?id={$row->id}")}}">{{$row->title}} </a></p>
                        </div>

                    @endforeach
                @endif
            @endif




        </div>
    </section>
@endsection
@section('content')
    <!-- Popular News -->
    <div class="column-one-third">
        <h5 class="line"><span>ពត៌មានពេញនិយម</span></h5>
        <div class="outertight">
            <ul class="block">

                @if(isset($dyn_slide[1]))
                    @if(count($dyn_slide[1])>0)
                        @foreach($dyn_slide[1] as $row)

                            <li>
                                <a href="{{url("single?id={$row->id}")}}"><img src="{{_getImage($row->image,"small")}}" alt="{{$row->description}}" class="alignleft" /></a>
                                <p>
                                    <span>{{\Carbon\Carbon::parse($row->created_at)->diffForHumans()}}</span>
                                    <a href="{{url("single?id={$row->id}")}}">{{$row->title}}</a>
                                </p>
                                <span class="rating"><span style="width:60%;"></span></span>
                            </li>
                        @endforeach
                    @endif
                @endif
            </ul>
        </div>

    </div>
    <!-- /Popular News -->

    <!-- Hot News -->
    <div class="column-one-third">
        <h5 class="line"><span>ពត៌មានថ្មីៗ</span></h5>
        <div class="outertight m-r-no">
            <ul class="block">

                @if(count($hot_news)>0)
                    @foreach($hot_news as $row)

                        <li>
                            <a href="{{url("single?id={$row->id}")}}"><img src="{{_getImage($row->image,"small")}}" alt="{{$row->description}}" class="alignleft" /></a>
                            <p>
                                <span>{{$row->created_at->diffForHumans()}}</span>
                                <a href="{{url("single?id={$row->id}")}}">{{$row->title}}</a>
                            </p>
                            <span class="rating"><span style="width:60%;"></span></span>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>

    </div>
    <!-- /Hot News -->

    <!-- Life Style -->
    <div class="column-two-third">
        <h5 class="line">
            <span>{{\App\Models\News::getCategoryTitle(1)}}</span>
            @php


            @endphp
            <div class="navbar">
                <a id="next1" class="next" href="#"><span></span></a>
                <a id="prev1" class="prev" href="#"><span></span></a>
            </div>
        </h5>

        <div class="outertight">

            @if(isset($get_home1[1]))
                @if(count($get_home1[1])>0)
                    @foreach($get_home1[1] as $row)

                        <img src="{{_getImage($row->image,"large")}}" alt="{{$row->description}}" />
                        <h6 class="regular"><a href="{{url("single?id={$row->id}")}}">{{$row->title}}</a></h6>
                        <span class="meta">{{\Carbon\Carbon::parse($row->created_at)->diffForHumans()}}</span>
                        <p>{{$row->description}}</p>

                    @endforeach
                @endif
            @endif
        </div>

        <div class="outertight m-r-no">

            <ul class="block" id="carousel">
                @if(isset($get_home1[0]))
                    @if(count($get_home1[0])>0)
                        @foreach($get_home1[0] as $row)
                            <li>
                                <a href="{{url("single?id={$row->id}")}}"><img src="{{_getImage($row->image,"small")}}" alt="{{$row->description}}" class="alignleft" /></a>
                                <p>
                                    <span>26 May, 2013.</span>
                                    <a href="{{url("single?id={$row->id}")}}">{{$row->title}}</a>
                                </p>
                                <span class="rating"><span style="width:60%;"></span></span>
                            </li>
                        @endforeach
                    @endif
                @endif
            </ul>
        </div>
    </div>
    <!-- /Life Style -->

    <!-- World News -->
    <div class="column-two-third">
        <h5 class="line">
            <span>{{\App\Models\News::getCategoryTitle(2)}}</span>
            <div class="navbar">
                <a id="next2" class="next" href="#"><span></span></a>
                <a id="prev2" class="prev" href="#"><span></span></a>
            </div>
        </h5>

        <div class="outerwide" >
            <ul class="wnews" id="carousel2">
                @if(isset($get_home2[1]))
                    @if(count($get_home2[1])>0)
                        @foreach($get_home2[1] as $row)
                            <li>
                                <img src="{{_getImage($row->image,"medium")}}" alt="{{$row->description}}" class="alignleft" />
                                <h6 class="regular"><a href="{{url("single?id={$row->id}")}}">{{$row->title}}</a></h6>
                                <span class="meta">{{\Carbon\Carbon::parse($row->created_at)->diffForHumans()}}</span>
                                <p>{{$row->description}}</p>
                            </li>

                        @endforeach
                    @endif
                @endif
            </ul>
        </div>

        <div class="outerwide">
            <ul class="block2">

                @if(isset($get_home2[0]))
                    @if(count($get_home2[0])>0)
                        @foreach($get_home2[0] as $row)
                            <li class="m-r-no">
                                <a href="{{url("single?id={$row->id}")}}"><img src="{{_getImage($row->image,"small")}}" alt="{{$row->description}}" class="alignleft" /></a>
                                <p>
                                    <span>{{\Carbon\Carbon::parse($row->created_at)->diffForHumans()}}</span>
                                    <a href="{{url("single?id={$row->id}")}}">{{$row->title}}.</a>
                                </p>
                                <span class="rating"><span style="width:60%;"></span></span>
                            </li>
                        @endforeach
                    @endif
                @endif
            </ul>
        </div>
    </div>
    <!-- /World News -->

    <!-- Popular News -->
    <div class="column-two-third">
        <div class="outertight">
            <h5 class="line"><span>{{\App\Models\News::getCategoryTitle(3)}}</span></h5>

            <div class="outertight m-r-no"style="margin-bottom: 10px;">
                <div class="flexslider">
                    <ul class="slides">
                        @if(isset($get_home3[0]))
                            @if(count($get_home3[0])>0)
                                @foreach($get_home3[0] as $row)
                                    <li>
                                        <img src="{{_getImage($row->image,"medium")}}" alt="MyPassion" />
                                        <p class="flex-caption"><a href="{{url("single?id={$row->id}")}}">{{$row->title}}</a> {{$row->description}} </p>
                                    </li>
                                @endforeach
                            @endif
                        @endif
                    </ul>
                </div>
            </div>

            <ul class="block">


                @if(isset($get_home3[0]))
                    @if(count($get_home3[0])>0)
                        @foreach($get_home3[0] as $row)
                            <li>
                                <a href="{{url("single?id={$row->id}")}}"><img src="{{_getImage($row->image,"small")}}" alt="{{$row->description}}" class="alignleft" /></a>
                                <p>
                                    <span>{{\Carbon\Carbon::parse($row->created_at)->diffForHumans()}}</span>
                                    <a href="{{url("single?id={$row->id}")}}">{{$row->title}}</a>
                                </p>
                                <span class="rating"><span style="width:100%;"></span></span>
                            </li>
                        @endforeach
                    @endif
                @endif
            </ul>
        </div>

        <div class="outertight m-r-no">
            <h5 class="line"><span>{{\App\Models\News::getCategoryTitle(4)}}</span></h5>

            <div class="outertight m-r-no"style="margin-bottom: 10px;">
                <div class="flexslider">
                    <ul class="slides">
                        @if(isset($get_home4[0]))
                            @if(count($get_home4[0])>0)
                                @foreach($get_home4[0] as $row)
                                    <li>
                                        <img src="{{_getImage($row->image,"medium")}}" alt="MyPassion" />
                                        <p class="flex-caption"><a href="#">{{$row->title}}</a> {{$row->description}} </p>
                                    </li>
                                @endforeach
                            @endif
                        @endif
                    </ul>
                </div>
            </div>

            <ul class="block">


                @if(isset($get_home4[0]))
                    @if(count($get_home4[0])>0)
                        @foreach($get_home4[0] as $row)
                            <li>
                                <a href="{{url("single?id={$row->id}")}}"><img src="{{_getImage($row->image,"small")}}" alt="{{$row->description}}" class="alignleft" /></a>
                                <p>
                                    <span>{{\Carbon\Carbon::parse($row->created_at)->diffForHumans()}}</span>
                                    <a href="{{url("single?id={$row->id}")}}">{{$row->title}}</a>
                                </p>
                                <span class="rating"><span style="width:100%;"></span></span>
                            </li>
                        @endforeach
                    @endif
                @endif
            </ul>
        </div>

    </div>
    <!-- /Popular News -->
@endsection