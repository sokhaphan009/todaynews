@extends('news.layout')

@section('title','Home')

@section('content')
    @if(isset($rows))

    <!-- Popular News -->
    <div class="column-two-third">

        <div class="outerwide">
            <ul class="block2">
                @foreach($rows as $row)

                <li  @if($loop->iteration  % 2 == 0)  class="m-r-no" @endif>
                    <a href="{{url("single-book?id={$row->id}")}}"><img src="{{_getImage($row->image,"small")}}" alt="{{$row->description}}" class="alignleft" /></a>
                    <p>
                        <span>{{\Carbon\Carbon::parse($row->created_at)->diffForHumans()}}</span>
                        <a href="{{url("single-book?id={$row->id}")}}">{{$row->title}}</a>
                    </p>
                    <span class="rating"><span style="width:80%;"></span></span>({{$row->view_count}})
                </li>
                @endforeach
            </ul>
        </div>

        <div class="pager">
            {!! $rows->links('vendor.pagination.news-list') !!}
        </div>

    </div>
    <!-- /Popular News -->
    @endif
@endsection