<?php
$detect = new \App\Helpers\Mobile_Detect();
?>
@extends('news.layout')

@section('title',isset($row)?$row->title:'news')

@section('fb_meta')
    @if(isset($row))
    <meta property="og:url"           content="{{app('request')->fullUrl()}}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{$row->title}}" />
    <meta property="og:description"   content="{{$row->description}}" />
    <meta property="og:image"         content="{{_getImage($row->image,"original")}}" />
    @endif

    <style>
        .article-content-single{
            color: black !important;
        }

        .article-content-single  ,.article-content-single div,.article-content-single p,.article-content-single ul li{
            font-size: 14px !important;
        }

        .article-content-single  img{
            max-width: 100%;
        }

    </style>
@endsection

@section('fb')
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
@endsection

@section('content')
    @if(isset($row))

        <!-- Single -->
        <div class="column-two-third single">
            <div class="flexslider">
                <h5 class="line"><span>{{$row->title}}</span>
                    @if(auth()->check())
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{url('admin/article/'.$row->id.'/edit')}}" style="color:red;">Edit</a> </span>
                     @endif
                </h5>

                <ul class="slides" style="margin-bottom: 10px;">
                    <li>
                        <img src="{{_getImage($row->image,($detect->isMobile() ? "medium" :"original"))}}" alt="{{$row->description}}" />
                    </li>
                </ul>
            </div>

            {{--<h6 class="title">{{$row->title}}</h6>--}}

            <span class="meta">{{\Carbon\Carbon::parse($row->created_at)->diffForHumans()}}.  <i class="icon-eye">{{$row->view_count}}</i>
            <div class="fb-share-button" data-href="{{app('request')->fullUrl()}}" data-layout="button_count" data-size="small"
                 data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(app('request')->fullUrl()) }}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>

                <div class="fb-like" data-href="{{app('request')->fullUrl()}}"
                     data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
            </span>

            <div class="article-content-single" style="margin-top: 20px;">
                {!! $row->content !!}
            </div>

            <div class="fb-comments" data-href="{{app('request')->fullUrl()}}" data-width="100%" data-numposts="5"></div>


            @if(isset($categories))
            <div class="relatednews">
                <h5 class="line"><span>Related News.</span></h5>
                <ul>
                    @if(count($categories)>0)
                        @foreach($categories as $r)
                            <li>
                                <a href="{{url("single?id={$r->id}")}}"><img

                                            src="{{_getImage($r->image,($detect->isMobile() ? "medium" : "small"))}}"

                                            alt="{{$r->description}}" /></a>
                                <p>
                                    <span>{{\Carbon\Carbon::parse($row->created_at)->diffForHumans()}}.</span>
                                    <a href="{{url("single?id={$r->id}")}}">{{$r->title}}</a>
                                </p>
                                <span class="rating"><span style="width:80%;"></span></span>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
            @endif
        </div>
        <!-- /Single -->

    @endif
@endsection