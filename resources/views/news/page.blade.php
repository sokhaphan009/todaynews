@extends('news.layout')

@section('title',isset($row)?$row->title:'news')

@section('content')
    @if(isset($row))

        <!-- Single -->
        <div class="column-two-third single">
    {{--        <div class="flexslider">
                <ul class="slides">
                    <li>
                        <img src="{{_getImage($row->image,"original")}}" alt="{{$row->description}}" />
                    </li>
                </ul>
            </div>--}}

            <h6 class="title">{{$row->title}}</h6>

        {{--    <span class="meta">{{\Carbon\Carbon::parse($row->created_at)->diffForHumans()}}.   \\   <a href="#">World News.</a>   \\   <a href="#">No Coments.</a></span>
--}}
            <div>
                {!! $row->content !!}
            </div>

     {{--       <ul class="sharebox">
                <li><a href=""><span class="twitter">Tweet</span></a></li>
                <li><a href=""><span class="pinterest">Pin it</span></a></li>
                <li><a href=""><span class="facebook">Like</span></a></li>
            </ul>--}}

        </div>
        <!-- /Single -->

    @endif
@endsection